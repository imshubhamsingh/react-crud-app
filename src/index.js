import React from "react";
import { render } from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import App from "./App";

import reducers from "./reducers";

const store = createStore(
  reducers,
  {
    userInfo: localStorage.getItem("userList")
      ? [...JSON.parse(localStorage.getItem("userList"))]
      : []
  },
  applyMiddleware(thunk)
);

render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
