import { ADD_USER, SEED_USERS } from "../actions/userInfoAction";

export default (state = [], action) => {
  switch (action.type) {
    case ADD_USER: {
      const userList = [
        ...state,
        {
          ...action.payload
        }
      ];
      localStorage.setItem("userList", JSON.stringify(userList));
      return [
        ...state,
        {
          ...action.payload
        }
      ];
    }
    case SEED_USERS: {
      return [...state, ...action.payload];
    }

    default:
      return state;
  }
};
