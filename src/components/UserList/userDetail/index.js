import React, { Component } from "react";
import "./userDetail.scss";

class userDetail extends Component {
  render() {
    return (
      <div className="user-detail-overlay">
        <div className="modal-content">
          <div className="modal-title">
            <div className="modal-heading-title">User Detail</div>
            <div className="cross">
              {" "}
              <i
                className="fa fa-times"
                onClick={() => this.props.toggleShow()}
              />{" "}
            </div>
          </div>
          <div className="userDetailsInfo">
            <div className="image-cropper">
              <img
                src={this.props.userDetails.imagePreviewUrl}
                className="rounded"
              />
            </div>
            <div>
              {this.props.userDetails.firstName}
              <br />
              {this.props.userDetails.email}
              <br />
              {this.props.userDetails.phone}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default userDetail;
