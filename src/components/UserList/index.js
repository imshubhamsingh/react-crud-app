import React, { Component } from "react";
import { connect } from "react-redux";
import UserCard from "./userCard";

import { seedUsers } from "../../actions/userInfoAction";

import "./userList.scss";

class user extends Component {
  state = {
    showDetails: false
  };
  async componentDidMount() {
    await this.props.seedUsers();
  }
  render() {
    //const { userDetails } = this.props;
    if (this.props.users.length > 0) {
      return (
        <div className="userList">
          {this.props.users.map((user, index) => (
            <UserCard key={index} user={user} />
          ))}
        </div>
      );
    }
    return <div className="center">No Users</div>;
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    users: state.userInfo
  };
};

export default connect(mapStateToProps, { seedUsers })(user);
