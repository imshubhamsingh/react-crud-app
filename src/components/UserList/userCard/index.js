import React, { Component } from "react";
import UserDetail from "../userDetail";

import "./userCard.scss";

class user extends Component {
  state = {
    toggle: false
  };
  toggleShow = () => {
    this.setState({
      toggle: !this.state.toggle
    });
  };

  render() {
    return (
      <div className="userCard">
        <div className="userImage">
          <img
            width="100%"
            src={this.props.user.imagePreviewUrl}
            alt={this.props.user.firstName}
          />
        </div>
        <div className="userDetails">
          <div className="userName">{this.props.user.firstName}</div>
          <div className="useremail">{this.props.user.email}</div>

          <div className="moreDetails">
            <div className="showDetails" onClick={() => this.toggleShow()}>
              Show More Details
            </div>
          </div>
        </div>
        {this.state.toggle ? (
          <UserDetail
            toggleShow={this.toggleShow}
            userDetails={this.props.user}
          />
        ) : (
          ""
        )}
      </div>
    );
  }
}

export default user;
