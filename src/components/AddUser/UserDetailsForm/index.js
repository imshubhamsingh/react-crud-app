import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import "./form.scss";

class UserDetailsForm extends Component {
  state = {
    firstName: "",
    email: "",
    file: "",
    imagePreviewUrl: "",
    phone: ""
  };
  submitForm = e => {
    e.preventDefault();
    const user = Object.assign({}, this.state);
    this.setState({
      firstName: "",
      email: "",
      file: "",
      imagePreviewUrl: "",
      phone: ""
    });
    this.props.sendUserDetails(user);
  };
  render() {
    return (
      <form onSubmit={e => this.submitForm(e)}>
        <input
          name="name"
          className="textInput"
          placeholder="Enter your name"
          onChange={e => {
            this.setState({ firstName: e.target.value });
          }}
          type="text"
        />
        <input
          name="email"
          className="textInput"
          placeholder="Enter your email"
          onChange={e => {
            this.setState({ email: e.target.value });
          }}
          type="email"
        />
        <input
          name="phone"
          className="textInput"
          placeholder="Enter your contact number"
          onChange={e => {
            this.setState({ phone: e.target.value });
          }}
          type="text"
        />
        <div className="file-upload">
          <label htmlFor="upload" className="filelabel">
            Select a pic
          </label>
          <input
            id="upload"
            className="fileinput"
            type="file"
            name="file-upload"
            onChange={e => {
              let reader = new FileReader();
              let file = e.target.files[0];

              reader.onloadend = () => {
                this.setState({
                  file: file,
                  imagePreviewUrl: reader.result
                });
              };
              reader.readAsDataURL(file);
            }}
          />
        </div>
        <input
          name="submit"
          className="submit"
          value="Submit"
          onChange={() => {}}
          type="submit"
        />
      </form>
    );
  }
}

export default UserDetailsForm;
