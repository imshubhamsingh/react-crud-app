import React, { Component } from "react";
import { connect } from "react-redux";
import "./addUsers.scss";

import { addUser } from "../../actions/userInfoAction";

import UserDetailsForm from "./UserDetailsForm";

class AddUsers extends Component {
  sendUserDetails = user => {
    this.props.addUser(user);
  };
  render() {
    return (
      <div className="user-add-form">
        <UserDetailsForm sendUserDetails={this.sendUserDetails} />
      </div>
    );
  }
}

export default connect(null, { addUser })(AddUsers);
