import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./header.scss";

class Header extends Component {
  render() {
    return (
      <nav className="nav">
        <Link to="/" className="brand">
          User <span>List</span>
        </Link>
        <div className="navbar">
          <Link to="/adduser" className="addUser">
            Add User
          </Link>
        </div>
      </nav>
    );
  }
}

export default Header;
