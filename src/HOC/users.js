import React, { Component } from "react";
import { connect } from "react-redux";

export default MainComponent => {
  const mapStateToProps = state => {
    return {};
  };

  class Users extends Component {
    state = {
      users: null
    };

    async componentDidMount() {}
    render() {
      //   if (this.state.users)
      //     return <MainComponent {...this.props} users={this.state.users} />;
      //   else return <div>Loading....</div>;
      return <MainComponent {...this.props} users={this.state.users} />;
    }
  }
  return connect(mapStateToProps, {})(Users);
};
