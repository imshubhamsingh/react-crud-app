import React, { Component } from "react";
import { Switch, Route, withRouter } from "react-router-dom";

import Header from "./components/Header";
import UsersList from "./components/UserList";
import AddUser from "./components/AddUser";

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route exact path="/" component={UsersList} />
          <Route exact path="/adduser" component={AddUser} />
        </Switch>
      </div>
    );
  }
}

export default App;
