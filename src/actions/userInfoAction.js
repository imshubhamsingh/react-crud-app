export const ADD_USER = "add_user";
export const DELETE_USER = "delete_user";
export const SEED_USERS = "seed_user";

export const addUser = user => async dispatch => {
  console.log(user);
  let output = {
    type: ADD_USER,
    payload: { ...user }
  };
  dispatch(output);
};

export const seedUsers = () => async dispatch => {
  let userList = localStorage.getItem("userList");
  userList = JSON.parse(userList);
  console.log(userList);
  let output = {
    type: SEED_USERS,
    payload: { ...userList }
  };
  dispatch(output);
};
